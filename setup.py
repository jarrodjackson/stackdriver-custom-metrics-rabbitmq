from setuptools import setup

import codecs
import os
import re


with open('README.md') as readme_file:
    README = readme_file.read().strip()

PROJECT = README.strip('#').split('\n')[0].strip().split()[0].lower()
DESCRIPTION = README.split('\n')[2]
HERE = os.path.abspath(os.path.dirname(__file__))


def find_version(*file_paths):
    # Open in Latin-1 so that we avoid encoding errors.
    # Use codecs.open for Python 2 compatibility
    with codecs.open(os.path.join(HERE, *file_paths), 'r', 'latin1') as f:
        version_file = f.read()

    # The version line must have the form
    # __version__ = 'ver'
    version_match = re.search(
        r"^__version__ = ['\"]([^'\"]*)['\"]", version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")

setup(
    name=PROJECT,
    description=DESCRIPTION,
    version=find_version('stackdriver.py'),
    author='David Kerr',
    author_email='david@davidmkerr.com',
    license='3-clause BSD',
    keywords='stackdriver plugin custom metrics',
    url='https://bitbucket.org/davidkerr/stackdriver-plugable-custom-metrics')
